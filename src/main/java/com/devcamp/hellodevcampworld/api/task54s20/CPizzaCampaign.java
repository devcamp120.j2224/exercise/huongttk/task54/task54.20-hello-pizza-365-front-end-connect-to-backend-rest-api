package com.devcamp.hellodevcampworld.api.task54s20;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CPizzaCampaign {
    @CrossOrigin
	@GetMapping("/devcamp-date")
	public String getDateViet(@RequestParam(name="name",defaultValue="Pizza Lover") String name) {
		DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        
        return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1", name, dtfVietNam.format(today));
	}
}