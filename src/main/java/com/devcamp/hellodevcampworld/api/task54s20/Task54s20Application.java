package com.devcamp.hellodevcampworld.api.task54s20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task54s20Application {

	public static void main(String[] args) {
		SpringApplication.run(Task54s20Application.class, args);
	}

}
