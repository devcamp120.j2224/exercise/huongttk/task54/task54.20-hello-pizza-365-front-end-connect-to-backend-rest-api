package com.devcamp.hellodevcampworld.api.task54s20;
    import org.springframework.web.bind.annotation.CrossOrigin;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RestController;
    @RestController

public class TestCampaign {
    @CrossOrigin
	@GetMapping("/devcamp-simple")
	public String simple() {
		return "test campaign";
	}

}
